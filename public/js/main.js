var boards = [
	{
		board:   '145963278326785149897412365483697512579821634261354897938546721652179483714238956',
		bitmask: '100111001101111100110100010011101100011010110001101110010001011001111101100111001'
	},
	{
		board:   '297563184358241769416879325879324651521796843643158297962417538135982476784635912',
		bitmask: '100011001110111001101100011101101110110101011011101101110001101100111011100110001'
	},
	{
		board:   '793841256582963147461725839817694325245137968639258714124579683378416592956382471',
		bitmask: '011000111111001100110111010100110001011010110100011001010111011001100111111000110'
	},
	{
		board:   '541897326287364195963152487126975834374218569895643271718426953452739618639581742',
		bitmask: '100001011101101111100101110110001110000111000011100011011101001111101101110100001'
	},
	{
		board:   '934528617681473925725691483319267854542819376867354291158946732473182569296735148',
		bitmask: '110100100101100001111011101110111011000010000110111011101110111100001101001001011'
	},
	{
		board:   '734192658918657243256834791163725984425968137879341526581479362397286415642513879',
		bitmask: '111001111000010100011011101011100111001010100111001110101110110001010000111100111'
	},
	{
		board:   '495732861713869524286451397358216749647983152129574683971648235562397418834125976',
		bitmask: '111001100101111011011110000010110111000101000111011010000011110110111101001100111'
	},
	{
		board:   '129654873437812659685793421958136742743289165261547398372961584516428937894375216',
		bitmask: '100011100011011100001101101101101111110000011111101101101101100001110110001110001'
	},
	{
		board:   '729461538548329716163857429896234157374516982251978643417683295982745361635192874',
		bitmask: '010011101010001011111011001001101011011010110110101100100110111110100010101110010'
	},
	{
		board:   '236158974879234615514967832453681729621795483798423156167542398345879261982316547',
		bitmask: '101111100011110011000010101010100011111111111110001010101010000110011110001111101'
	},
];

$(function() {
	
	for(i in boards) {
		$('#game-select').append('<option value="'+i+'">Board '+ (parseInt(i)+1) + '</option>');
	}
	
	
	for(var i=0;i<9;i++) {
		var tr = $('<tr/>');
		for(var j=0;j<9;j++) {
			var box = 3* parseInt(i/3) + parseInt(j/3);
			tr.append('<td><input data-x="'+j+'" data-y="'+i+'" data-box="'+box+'" /></td>');
		}
		$('#sudoku').append(tr);
	}
	
	Sudoku.startGameOrLoad(boards[0].board,boards[0].bitmask);
	
	$('#start-game').on('click',function(e){
		e.preventDefault();
		var v = $('#game-select').find(':selected').val();
		Sudoku.startGame(boards[v].board,boards[v].bitmask);
	});
	
	$('#sudoku input').on('focus',function(e) {
		e.preventDefault();
		Sudoku.move($(this).attr('data-x'),$(this).attr('data-y'));
		$(this).trigger('blur');
	});
	
	$(document).on('keydown',function(e){
		var code = e.keyCode;
		
		// arrows
		if(code >= 37 && code <= 40) {
			e.preventDefault();
		}
		
		if(code === 37)
			Sudoku.moveLeft();
		if(code === 38)
			Sudoku.moveUp();
		if(code === 39)
			Sudoku.moveRight();
		if(code === 40)
			Sudoku.moveDown();
		
		if(code >= 48 && code <= 57) {
			e.preventDefault();
			Sudoku.setValue(code-48);
		}
		
		if(code === 80) { // P
			if(Sudoku.isPaused())
				$('#resume').trigger('click');
			else
				$('#pause').trigger('click');
		}
		
		if(code == 72) { // H
			$('#hint').trigger('click');
		}
		
	});
	
	function redrawTime() {
		
		var t = Sudoku.getTime();

		function pad2(number) {
			return (number < 10 ? '0' : '') + number
		}
		
		$('#time').html(pad2(parseInt(t/60)) +':' + pad2(parseInt(t%60)) );
		
		setTimeout(redrawTime,500);
	}
	redrawTime();
	
	$('#hint').on('click',function(e){
		e.preventDefault();
		if(Sudoku.isPaused())
			return;
		$('#hint-box').html(Sudoku.hint()).fadeIn(200,function() {$(this).fadeOut(200); });
	});
	
	$('#undo').on('click',function(e){
		e.preventDefault();
		Sudoku.undo();
	});
	
	$('#redo').on('click',function(e){
		e.preventDefault();
		Sudoku.redo();
	});
	
	
	// pause button
	$('#pause').on('click',function(e){
		e.preventDefault();
		Sudoku.pause();
		$('#appla').fadeIn();
		$('#container').addClass('blur');
	});
	
	$('#resume').on('click',function(e){
		e.preventDefault();
		Sudoku.resume();
		$('.blur').removeClass('blur');
		$('#appla').fadeOut();
	});
	
});