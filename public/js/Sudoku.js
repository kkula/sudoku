// chunk string into chunks n chars long.
String.prototype.chunk = function ( n ) {
    if ( !this.length ) {
        return [];
    }
    return [ this.slice( 0, n ) ].concat( this.slice(n).chunk(n) );
};

// sets new cookie
function setCookie(c_name, value, exdays)
{
	value = JSON.stringify(value);
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

// gets cookie
function getCookie(c_name)
{
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++)
	{
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name)
		{
			return JSON.parse(unescape(y));
		}
	}
}

// sudoku main object
var Sudoku = function() {
	
	var fullBoard = [];
	var bitmask = [];
	var board = [];
	var x = 0;
	var y = 0;
	var startTime = null;
	var agregatedTime = 0;
	var pauseStart = null;
	var history = [];
	var historyRedo = [];
	var el = '#sudoku';
	
	function clearBoard() {
		board = [];
		for(var i = 0; i < 9; i++) {
			board.push([]);
			for (var j = 0; j < 9; j++) {
				board[i].push(0);
			}
		}
	}
	
	clearBoard();
	
	// returns element on (x,y) coords.
	function getEl(x,y) {
		return $(el).find('[data-x="'+x+'"][data-y="'+y+'"]');
	}
	
	// returns currently highlighted element
	function getCurrent() {
		return getEl(x,y);
	}
	
	// returns all elements on the board
	function getAllEl() {
		return $(el).find('input');
	}
	
	// returns elements on the current row
	function getCurrentRow() {
		return $(el).find('[data-x="'+x+'"]');
	}
	
	// returns element on the current col
	function getCurrentCol() {
		return $(el).find('[data-y="'+y+'"]');
	}
	
	// returns elements in the current box
	function getCurrentBox() {
		var box = getCurrent().attr('data-box');
		return $(el).find('[data-box="'+box+'"]');
	}
	
	// saves the game
	function saveGame() {
		setCookie('sudoku',{
			fullBoard: fullBoard,
			bitmask: bitmask,
			board: board,
			x: x,
			y: y,
			startTime: startTime,
			agregatedTime: agregatedTime,
			pauseStart: pauseStart,
			history: history,
			historyRedo: historyRedo
		});
	}
	
	// loads game from the cookie
	function cookieLoadGame() {
		var d = getCookie('sudoku');
		fullBoard = d.fullBoard;
		bitmask = d.bitmask;
		board = d.board;
		x = d.x;
		y = d.y;
		startTime = d.startTime;
		pauseStart = d.pauseStart;
		agregatedTime = d.agregatedTime;
		history = d.history;
		historyRedo = d.historyRedo;
	}
	
	// fills the board
	function fillBoard() {
		getAllEl().removeClass('disabled').val('');
		for (var i = 0; i < 9; i++) {
			for (var j = 0; j < 9; j++) {
				if (bitmask[i][j] === '0') {
					getEl(i, j).val(fullBoard[i][j]).addClass('disabled');
				} else {
					if (board[i][j])
						getEl(i, j).val(board[i][j]);
				}
			}
		}
	}
	
	// highlights columns that are duplicates in each row,col,box...
	function highlightDuplicates() {
		getAllEl().removeClass('error');
		for(var i=0;i<9;i++) {
			hlDup($(el).find('[data-x="'+i+'"]'));
			hlDup($(el).find('[data-y="'+i+'"]'));
			hlDup($(el).find('[data-box="'+i+'"]'));
		}
	}
	
	// checks if sudoku is solved.
	function checkSolved() {
		for(var i=0;i<9;i++) {
			for(var j=0;j<9;j++) {
				if(bitmask[i][j] !== '0' && parseInt(board[i][j]) !== parseInt(fullBoard[i][j])) {
					return false;
				}
			}
		}
		return true;
	}
	
	// returns current solve time.
	function getTm() {
		if (!!pauseStart)
			return agregatedTime / 1000;
		return (agregatedTime + new Date().getTime() - startTime) / 1000;
	}
	
	// gihlights duplicates in the set of elements
	function hlDup(els) {
		var v = [];
		for(var i=0;i<10;i++)
			v.push(0);
		els.each(function() {
			v[$(this).val()]++;
		});
		els.each(function(){
			if(v[$(this).val()] >= 2)
				$(this).addClass('error');
		});
	}
	
	// highlights current element and current row/col/box.
	function redrawHighlight() {
		getAllEl().removeClass('current').removeClass('highlighted');
		getCurrent().addClass('current');
		getCurrentRow().addClass('highlighted');
		getCurrentCol().addClass('highlighted');
		getCurrentBox().addClass('highlighted');
	};
	
	// redraws field (x,y) coords.
	function redrawField(x,y) {
		if(board[x][y] === 0) {
			getEl(x,y).val('');
		} else {
			getEl(x,y).val(board[x][y]);
		}
		highlightDuplicates();
		if(checkSolved()) {
			alert('Solved!');
		}
	}
	
	// public methods.
	return {
		// starts new game
		startGame: function(newBoard,newBitmask) {
			newBoard = newBoard.chunk(9);
			bitmask = newBitmask.chunk(9);
			clearBoard();
			
			fullBoard = newBoard;
			x=0;
			y=0;
			fillBoard();
			redrawHighlight();
			highlightDuplicates();
			startTime = (new Date()).getTime();
			agregatedTime = 0;
			pauseStart = null;
			history = [];
			historyRedo = [];
			
			saveGame();
		},
		// starts new game or loads one from the cookie if exists
		startGameOrLoad: function(newBoard,newBitmask) {
			if(getCookie('sudoku')) {
				cookieLoadGame();
				fillBoard();
				highlightDuplicates();
				redrawHighlight();
				checkSolved();
			} else {
				this.startGame(newBoard,newBitmask);
			}
		},
		moveLeft: function() {
			this.move(x-1,y);
		},
		moveRight: function() {
			this.move(x+1,y);
		},
		moveDown: function() {
			this.move(x,y+1);
		},
		moveUp: function() {
			this.move(x,y-1);
		},
		move: function(nx,ny) {
			if(this.isPaused())
				return;
			
			nx = parseInt(nx);
			ny = parseInt(ny);
			if(nx >= 0 && nx < 9)
				x = nx;
			if(ny >= 0 && ny < 9)
				y = ny;
			redrawHighlight();
			saveGame();
		},
		pause: function() {
			pauseStart = (new Date()).getTime();
			agregatedTime += pauseStart - startTime;
		},
		resume: function() {
			pauseStart = null;
			startTime = new Date().getTime();
		},
		getTime: function() {
			return getTm();
		},
		isPaused: function() {
			return !!pauseStart;
		},
		hint: function() {
			return fullBoard[x][y];
		},
		setValue: function(v) {
			if(this.isPaused())
				return;
			if(bitmask[x][y] === '0')
				return;
			
			historyRedo = [];
			history.push({
				x: x,
				y: y,
				oldValue: board[x][y],
				newValue: v
			});
			
			board[x][y] = v;
			redrawField(x,y);
			saveGame();
		},
		undo: function() {
			if(!history.length)
				return;
			var h = history.pop();
			historyRedo.push(h);
			this.move(h.x,h.y);
			board[x][y] = h.oldValue;
			redrawField(x,y);
			saveGame();
		},
		redo: function() {
			if(!historyRedo.length)
				return;
			var h = historyRedo.pop();
			history.push(h);
			this.move(h.x,h.y);
			board[x][y] = h.newValue;
			redrawField(x,y);
			saveGame();
		},
		loadGame: function() {
			cookieLoadGame();
		},
		fill: function() {
			for(var i=0;i<9;i++) {
				for(var j=0;j<9;j++) {
					if(bitmask[i][j] === '1') {
						this.move(i,j);
						this.setValue(fullBoard[i][j]);
					}
				}
			}
		}
	};
}();